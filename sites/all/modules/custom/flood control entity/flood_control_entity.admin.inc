<?php

/**
 * Implements hook_form().
 */
function flood_control_entity_form($form, &$form_state, $flood_control_entity) {
  $form['information'] = array(
    '#type' => 'markup',
    '#markup' => flood_control_entity_information($form_state),
  );

  $form_state['flood_control_entity'] = $flood_control_entity;
  field_attach_form('flood_control_entity', $flood_control_entity, $form, $form_state);

  $form['actions'] = array(
    '#type' => 'actions',
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  $form['#validate'][] = 'flood_control_entity_form_validate';

  return $form;
}

/**
 * Return information about node views.
 */
function flood_control_entity_information($form_state) {
  $node_views = $form_state['build_info']['args'][0]->nid;
  $views_user = $form_state['build_info']['args'][0]->uid;
  $views_date = format_date($form_state['build_info']['args'][0]->created);
  $node = node_load($node_views);
  $user = user_load($views_user);
  $return = "<div>";
  $return .= l($node->title, "node/" . $node->nid);
  $return .= " views ";
  if ($user->uid) {
    $return .= l($user->name, "user/" . $user->uid);
  }
  else {
    $return .= t("Anonymous");
  }
  $return .= t(" in @time", array('@time' => $views_date));
  $return .= "</div>";

  return $return;
}

/**
 * Form API validate callback for the flood control entity form
 */
function flood_control_entity_form_validate(&$form, &$form_state) {
  $flood_control_entity = $form_state['flood_control_entity'];

  // Notify field widgets to validate their data.
  field_attach_form_validate('flood_control_entity', $flood_control_entity, $form, $form_state);
}

/**
 * Implements hook_form_submit().
 */
function flood_control_entity_form_submit(&$form, &$form_state) {
  // $flood_control_entity = entity_ui_form_submit_build_entity($form, $form_state);
  $flood_control_entity= entity_ui_controller('flood_control_entity')->entityFormSubmitBuildEntity($form, $form_state);
  $flood_control_entity->save();
  $form_state['redirect'] = 'admin/structure/flood';
  //field_attach_submit($flood_control_entity, $flood_control_entity, $form, $form_state);
}
