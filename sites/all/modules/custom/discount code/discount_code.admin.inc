<?php

/**
 * Implements hook_settings().
 * Configuration settings page.
 *
 * @return
 *    Array of the system settings form.
 */
function discount_code_settings() {
  $path = drupal_get_path('module', 'discount_code');

  $form['discount_code_settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'Plugin settings',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['discount_code_settings']['hello_message'] = array(
    '#type' => 'textarea',
    '#size' => 100,
    '#title' => t('Hello message'),
    '#description' => t('This is hello message, where user get his discount code. Use tokens for render
     user name and unique code. You can writing [custom : userName] for name and [custom : discountCode] for code'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return system_settings_form($form);
}

