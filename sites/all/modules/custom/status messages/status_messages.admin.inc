<?php

/**
 * Implements hook_settings().
 * Configuration settings page.
 *
 * @return
 *    Array of the system settings form.
 */
function status_messages_settings() {
  $path = drupal_get_path('module', 'status_messages');
  $form['#attached']['js'][]  = $path . '/js/admin.js';
  $form['#attached']['library'][] = array('system', 'farbtastic');

  $form['status_messages_settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'Plugin settings',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['status_messages_settings']['wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => t('Color settings'),
  );

  $form['status_messages_settings']['wrapper']['colorwheel_bg'] = array(
    '#type'   => 'textfield',
    '#size'   => 8,
    '#prefix' => '<div><div class="colorwheel-farbtastic"></div>',
    '#suffix' => '</div>',
    '#default_value' => variable_get('colorwheel_bg', ''),
    '#description' => t('Background color'),
    '#element_validate' => array('status_messages_color_validate'),
  );

  $form['status_messages_settings']['status_messages_window_title'] = array(
    '#type' => 'textfield',
    '#size' => 20,
    '#title' => t('Window title'),
    '#description' => t('The window title.'),
  );


  $form['status_messages_settings']['status_messages_window_size'] = array(
    '#type' => 'textfield',
    '#size' => 10,
    '#title' => t('Window size'),
    '#description' => t('The size window.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  // @todo: drupal_settings_form
  return system_settings_form($form);
}

/**
 * Color validate
 *
 * @param $element
 */
function status_messages_color_validate($element) {
  $value = $element['#value'];
  preg_match('/#([a-f]|[A-F]|[\d]){3}(([a-f]|[A-F]|[\d]){3})?\b/', $value, $match);
  if (empty($match[0])) {
    form_error($element, t('Please enter the valid color (use color wheel).'));
  }
}

