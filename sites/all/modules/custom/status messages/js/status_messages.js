Drupal.theme.prototype.window = function(title) {
  var output =
    '<div class="popup-overlay" id = "popup">' +
    '<div class="pop-up" id = "popup-up"><div class="message-frame"><h1>' + title +
    '</h1>' + '<a class="close" title="close" href=""</a>' + '</div></div></div>';
  return output;
};
Drupal.theme.prototype.message = function(key) {
  return '<li>' + key + '</li>';
};
Drupal.theme.prototype.themeColor = function(messageType, message) {
  return '<div class ="message' + messageType + '"><ul>' + message + '</ul></div>';
};
/**
 * Generate pop-up window for status message
 */
(function($) {
  $(document).ready(function() {
    var key;
    var color;
    var mess;
    if(Drupal.settings.status_messages.windowMessages != "") {
      $('body').append(Drupal.theme('window', Drupal.settings.status_messages.windowTitle));
      var messagesStatus = "";
      var messagesError = "";
      for (key in Drupal.settings.status_messages.windowMessages) {
        for(mess in Drupal.settings.status_messages.windowMessages[key]) {
          if (key == 'status') {
            messagesStatus += Drupal.theme('message', Drupal.settings.status_messages.windowMessages[key][mess]);
            console.log(Drupal.settings.status_messages.windowMessages[key][mess]);
          }
          if (key == 'error') {
            messagesError += Drupal.theme('message', Drupal.settings.status_messages.windowMessages[key][mess]);
          }
        }
      }
      $('.pop-up').append(Drupal.theme('themeColor', 'Error', messagesError));
      $('.pop-up').append(Drupal.theme('themeColor', 'Status', messagesStatus));
      $('.pop-up').css('background-color', Drupal.settings.status_messages.windowColor);
      $('.pop-up').css('width', Drupal.settings.status_messages.windowSize);
      $('.messageStatus').css('color', '#2f852f');
      $('.messageError').css('color', '#b6251b');
    }
    $(".close").click(function () {
      $("#popup").fadeOut(300);
      $("#popup-up").fadeOut();
      setTimeout(function() {
        $(".popup_overlay").css('display', 'none');
      }, 300);
      return false;
    });
    $(".popup-overlay").click(function () {
      $("#popup").fadeOut(300);
      $("#popup-up").fadeOut();
      setTimeout(function(){
        $(".popup_overlay").css('display', 'none');
      }, 300);
      return false;
    });
    var window = document.getElementById('popup-up');
    $(window).mousedown(function(e) {
      function getCoords(window) { // кроме IE8-
        var box = window.getBoundingClientRect();
        return {
          top: box.top + pageYOffset,
          left: box.left + pageXOffset
        };
      }
      var coords = getCoords(window);
      var shiftX = e.pageX - coords.left;
      var shiftY = e.pageY - coords.top;
      $(window).css('position', 'absolute');
      moveAt(e);
      document.body.appendChild(window);
      $(window).css('zIndex', 1000);
      function moveAt(e) {
        $(window).css('left', e.pageX - shiftX + 'px');
        $(window).css('top', e.pageY - shiftY + 'px');
      }
      $(window).mousemove(function(e){
        if(e.which == 1) {
          moveAt(e);
        }
      });
      $(window).mouseup = (function() {
        document.mousemove = null;
        $(window).mouseup = null;
      });
      window.ondragstart = function() {
        return false;
      };
    });
  });
}(jQuery));