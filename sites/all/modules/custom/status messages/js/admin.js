/**
 * Generate admin page, select color, size, title.
 */
(function ($) {
    /**
     * Color wheel js functions.
     */
    Drupal.behaviors.status_messages = {
        attach: function(context, settings) {
            var current = this;
            var div = $("div.colorwheel-farbtastic", context),
                input = $('input.form-text', div.parent()),
                farb  = $.farbtastic(div),
                color = input.val();

            if (color != '') {
                farb.setColor(color);
                input.css({
                    'background-color': color,
                    'color': current.invertColor(color)
                });
            }

            farb.linkTo(function(color) {
                input.css({
                    'background-color': color,
                    color: current.invertColor(color)
                }).val(color);
            });

            input.unbind().bind('keyup change', function() {
               farb.setColor($(this).val());
            });
        },
        invertColor: function (inputcolor) {
            var color = inputcolor.substring(1);
            color = parseInt(color, 16);
            color = 0xFFFFFF ^ color;
            color = color.toString(16);
            color = ("000000" + color).slice(-6);
            return "#" + color;
        }
    };
})(jQuery);