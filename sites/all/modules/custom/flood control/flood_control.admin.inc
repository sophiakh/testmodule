<?php

/**
 * Configuration settings page.
 *
 * @return
 *    Array of the system settings form.
 */
function flood_control_node_view_count() {
  $form['flood_control_node_view_count'] = array(
    '#type' => 'fieldset',
    '#title' => 'Plugin settings',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['flood_control_node_view_count']['node_table'] = array(
    '#type' => 'item',
    '#title' => t('Node list with count views'),
    '#markup' => flood_control_table_view_count(),
  );

  return drupal_render($form);
}

/**
 * Generate html table with title nodes and views count.
 *
 * @return string
 */
function flood_control_table_view_count() {
  $query = db_select('node_views', 'n');
  $query->addExpression('COUNT(n.nid)', 'vote_sum');
  $query->innerJoin('node', 'u', 'u.nid = n.nid');
  $query->fields('u', array('title'));
  $query->fields('n', array('id'));
  $query->fields('n', array('nid'));
  $query->groupBy('n.nid');
  $nodes = $query->execute();
  $header = array('Title', 'Count');
  $rows = array();

  foreach ($nodes as $node) {
    $rows[] = array(
      l(t($node->title), "/node/" . $node->nid),
      l(t($node->vote_sum), "admin/config/user-interface/flood/edit/" . $node->nid),
    );
  }
  $output = theme('table', array('header' => $header, 'rows' => $rows));
  return $output;
}

/**
 * Render page with node view details.
 *
 * @param $node
 *   Param of the id node in database.
 *
 * @return string
 */
function flood_control_node_view_statistic($node) {
  $form['flood_control_node_view_statistic'] = array(
    '#type' => 'fieldset',
    '#title' => 'Statistic node views',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['flood_control_node_view_statistic']['node_statistic_table'] = array(
    '#type' => 'item',
    '#markup' => flood_control_table_view_statistic($node->nid),
  );

  return drupal_render($form);
}

/**
 * Generate html table with users name and time, who views this node.
 *
 * @param $nid
 *   Nodes id.
 *
 * @return string
 */
function flood_control_table_view_statistic($nid) {
  //sql query for select user name on users table and time view on node_views.
  $query = db_select('node_views', 'n');
  $query->innerJoin('users', 'u', 'u.uid = n.uid');
  $query->fields('u', array('name'));
  $query->fields('n', array('created'));
  $query->fields('u', array('uid'));
  $query->condition('n.nid', $nid , '=');
  $nodes = $query->execute()->fetchAll();
  $header = array('Username', 'Created');
  $rows = array();

  foreach ($nodes as $node) {
    if($node->uid == 0) {
      $userName = 'anonymous';
    }
    else {
      $userName = l(t($node->name), "/user/" . $node->uid);
    }
    $rows[] = array(
      $userName,
      format_date($node->created),
    );
  }

  $output = theme('table', array('header' => $header, 'rows' => $rows));
  return $output;
}
